## 6/22
- 最適化
 + グローバル
   * 基本ブロックをまたがる
   * フロー解析が必要 data-flow analisys, control-
 + ローカル
   * 基本ブロックの中で
   * **peephole 最適化**
     パターンマッチしなくなるまで再帰的に当たっていく

- ベンチマーク用の適当な関数
  + 竹内関数
  + fib

- 次までに
  + フロー解析をするかどうか(global/local optimization)
  + その中間形式
  + 命令の挿入/削除library

## 7/6
- 最適化の何を
  + 定数の伝播
  + peephole 
  + d/u analysis

- online partial evaluation
  + 部分評価､一部をinterprete
  + 実行と解析を同時におこなう
  + また別のコンパイラのような働きをする｡ターゲットが同じという感じですか｡
- offline partial evaluation
  1. static/dynamic ラベルをつける
  2. ラベルの推論
  3. 別言語Lにtranslate
  4. Lを型に基づきpartial evaluate
  + 実行と解析を分ける
  + 再帰構造などではd/sが決定不能となり､困るなどがある

言語AからBへのtranspile時の､transpileされたBに対して有効､他は実はあまりアレ
