#optimizer
##できてる
- constant folding
- constant propagation
- unreachable blocks removal

##バグあり
- dead-code elimination
- unused constants and closures removal
- function inlining

##考えてる
- code motion

## 今後
- refactoring
- bug fix
- code motionの実装?
- レポートを書き進める
- スライド(1月〜)

# sub projects
## LASM, Lua vm ASseMbly-like language
DSL to be compiled to Lua VM bytecode
```
Lasm      <- Main (Space * Block)*
Main      <- "main" ':' Space Body
Block     <- Label ':' Space Body
Body      <- InsList Space ('{' Space ConstList Space '}')?
InsList   <- Ins (Space Ins)*
Ins       <- ("CLOSURE" Space Number Space Label) / Opcode Space Operand
Opcode    <- [A-Z]+
Operand   <- Number (Space Number){,2}
ConstList <- Const (Space Const)*
Const     <- ('"' ([:print:] - '"')* '"') / Number
Label     <- [a-zA-Z0-9]+
Number    <- '-'? (("0x" [0-9a-f]+) / [0-9]+ ('.' [0-9]+)?)
Space     <- (' ' / '\n' / '\t' / Comment)*
Comment   <- "--" (. - '\n')*
```

## Graphize
Lua VM Instructions Control Graph Visualizer
![](///home/nymphium/works/joutoku/joutoku2016/text/graphize.png)

## moonstep
step-executionable Lua VM

```
$ cat << LUA | luac -l -l -
print "hello, world"
LUA

main <stdin:0,0> (4 instructions at 0xf58760)
0+ params, 2 slots, 1 upvalue, 0 locals, 2 constants, 0 functions
        1       [1]     GETTABUP        0 0 -1  ; _ENV "print"
        2       [1]     LOADK           1 -2    ; "hello, world"
        3       [1]     CALL            0 2 1
        4       [1]     RETURN          0 1
constants (2) for 0xf58760:
        1       "print"
        2       "hello, world"
locals (0) for 0xf58760:
upvalues (1) for 0xf58760:
        0       _ENV    1       0
$ moonstep luac.out
[1]> d
{
  pc = 0,
  reg = {}
}
[1]> n
[2]> n
[3]> n
hello, world
[4]> d
{
  pc = 3,
  reg = { "hello, world",
    [0] = <function 1>
  }
}
[4]> n
[(dead)]> q
```