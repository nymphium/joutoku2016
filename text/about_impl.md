optimizations:
===
- constant fold
	+ 四則演算､`>>` `<<` `&` `~`(ビット演算)など､両項の型が`number`のみ
	+ `CONCAT`(文字列の結合)
	+ `LEN` (文字列､tableの長さ)
- constnant propagation?
  `MOVE`命令が指している命令を`MOVE`命令にいれる｡`MOVE`命令と二項演算命令(constnat foldingが対象にしてるもの)を対象にしている｡

	```
MOVE 3 0
......
MOVE 5 3
→
MOVE 3 0
......
MOVE 3 0 // replaced
    ```

- dead-code elimination
	D/U Chainでusedが空のものを消す｡条件式も評価して削除したりしている
	```
LOADBOOL 3 0
TEST 0 3
JMP 0 5
LOADK 4 10
......
→
LOADBOOL 3 0
JMP 0 5 // `TEST` removed
LOADK 4 10 // unreachable
    ```
- function inlining
	`RETURN` が分岐していない､再帰関数でない､という制限あり､前者はそのうち取り除く
- unreachable block removal
- unused constants and closures removal
	constants, closureはconstant list, prototype listに含まれており､これから不要なものを削除することで実行形式のファイルサイズを縮小できる｡

## 最適化の順番は?
### 暫定
`dead_elim cst_prop cst_fold func_inline unreachable_remove fnblock`
1. unreachable block removal
	まず不要なブロックを消すことで後続の最適化を楽にする｡
2. function inlining
	inliningを先にすることで､できうる不要な部分などをあとで片付けてもらう､constant foldingなども関数の戻り値を使うかもなので
3. constant folding
4. constant propagation
5. dead-code elimination
最後に不要なところを消す｡

これをトップレベルから各closureに再帰的におこなっていく｡