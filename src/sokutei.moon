sock = require'socket'

f = loadfile arg[1]

times = tonumber(arg[2]) or 10

time = 0

for _ = 1, times
	t = sock.gettime!
	f!
	t_ = sock.gettime!
	time += (t_ - t)

print "%.3f"\format (time / times)

