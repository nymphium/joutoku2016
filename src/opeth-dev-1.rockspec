package = "opeth"
version = "dev-1"
source = {
   url = "*** please add URL for source tarball, zip or repository here ***"
}
description = {
   homepage = "*** please enter a project homepage ***",
   license = "*** please specify a license ***"
}
dependencies = {
	"inspect",
	"lpeg",
	"moonscript",
	"argparse",

}
build = {
   type = "builtin",
   modules = {},
	install = {
		lua = {
		},
		bin = {
			lasmc = [[opeth/bin/lasmc]],
			lvis = [[opeth/bin/lvis]],
			moonstep = [[opeth/bin/moonstep]]
		}
	}
}
