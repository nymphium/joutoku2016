#!/usr/bin/env moon

import read, Reader from require'opeth.bytecode.reader'
import write, Writer from require'opeth.bytecode.writer'
parser = require'bf.parser'

newbytecode = read Reader nil, string.dump (-> print), true

local lasmcode

if arg[1] == "-"
	lasmcode = ""

	while true
		line = io.read!
		unless line
			break
		lasmcode ..= line
else
	io.close with f = assert io.open arg[1]
		lasmcode = f\read "*a"

linstruction, lconstant = parser lasmcode

with newbytecode.fnblock
	.upvalue = {
		{
			instack: 1
			reg: 0
		}
	}
	.chunkname = ""
	.regnum = "%02x"\format 255
	.instruction = linstruction
	.constant = lconstant
	.line = {
		defined: "00000000"
		lastdefined: "00000000"
	}

(=> @\close!) with Writer 'bf.out'
	\write newbytecode

