MOVE     = (a, b)    -> {a, b, 0, op: "MOVE"}
LOADK    = (a, b)    -> {a, b,    op: "LOADK"}
ADD      = (a, b, c) -> {a, b, c, op: "ADD"}
SUB      = (a, b, c) -> {a, b, c, op: "SUB"}
CALL     = (a, b, c) -> {a, b, c, op: "CALL"}
JMP      = (to)      -> {0, to,   op: "JMP"}
EQ       = (a, b, c) -> {a, b, c, op: "EQ"}
RETURN   = (a, b, c) -> {a, b, c, op: "RETURN"}
GETTABUP = (a, k)    -> {a, 0, k, op: "GETTABUP"} -- a = _ENV[K(C)]
GETTABLE = (a, b, c) -> {a, b, c, op: "GETTABLE"}
nth = (t, c) -> for i = 1, #t do return i if t[i].val == c
K = (k) -> k - 1
RK = (k) -> k > 0 and -k or K(k)

tape_boundary = {start: 4, end: 252}
looking_ptr = tape_boundary.start
call_ptr = tape_boundary.end + 1

-- position of register
READ_PTR  = 0
WRITE_PTR = 1
CHAR_PTR  = 2
BYTE_PTR  = 3

(input) ->
	jmp_stack = {}
	ins_idx = 0

	constant = [({
		val: e
		type: switch type e
				when "number" then (math.tointeger e) and 0x13 or 0x03
				when "string" then (#e > 255) and 0x14 or 0x04
		}) for e in *{"io", "string", "read", "write", "char", "byte", 1, 0}]

	-- position of constant table
	ZERO = nth constant, 0
	ONE  = nth constant, 1

	instruction = {
		GETTABUP 0, -(nth constant, "io")
		GETTABUP 2, -(nth constant, "string"),
		GETTABLE WRITE_PTR, 0, -(nth constant, "write") -- R(1) = io.write
		GETTABLE READ_PTR,  0, -(nth constant,  "read") -- R(0) = io.read
		GETTABLE BYTE_PTR,  2, -(nth constant,  "byte") -- R(3) = string.byte
		GETTABLE CHAR_PTR,  2, -(nth constant,  "char") -- R(2) = string.char
	}

	insert = =>
		table.insert instruction, @
		ins_idx += 1

	-- initialize R(i) to 0
	insert (LOADK i, ZERO - 1) for i = tape_boundary.start, tape_boundary.end

	for c in input\gmatch'.'
		switch c
			when ">"
				error "pointer limit (ptr <= #{tape_boundary.end})" if looking_ptr > tape_boundary.end
				looking_ptr += 1
			when "<"
				error "pointer limit (ptr >= #{tape_boundary.start})" if looking_ptr < tape_boundary.start
				looking_ptr -= 1
			when "+"
				-- (*looking_ptr)++
				insert ADD looking_ptr, looking_ptr, -ONE
			when "-"
				-- (*looking_ptr)--
				insert SUB looking_ptr, looking_ptr, -ONE
			when ","
				-- looking_ptr = io.read!
				insert MOVE call_ptr , BYTE_PTR
				insert MOVE call_ptr + 1, READ_PTR
				insert LOADK call_ptr + 2, ONE - 1
				insert CALL call_ptr + 1, 2, 2
				insert CALL call_ptr, 2, 2
				insert MOVE looking_ptr, call_ptr
			when "."
				-- io.write string.char *now_ptr
				insert MOVE call_ptr, WRITE_PTR
				insert MOVE call_ptr + 1, CHAR_PTR
				insert MOVE call_ptr + 2, looking_ptr
				insert CALL call_ptr + 1, 2, 2
				insert CALL call_ptr, 2, 1
			when "["
				jmp = JMP!
				insert EQ 1, looking_ptr, -ZERO
				insert jmp
				table.insert jmp_stack, {line:ins_idx, jmp} -- determine where to JMP when corresponding "]"
			when "]"
				jmpback = table.remove jmp_stack
				jx = ins_idx - jmpback.line
				jmpback[1][2] = jx
				insert EQ 0, looking_ptr, -ZERO
				insert JMP -jx - 4
			else
				io.stderr\write "ignore #{c}\n"

	insert RETURN 0, 1, 0
	instruction, constant

