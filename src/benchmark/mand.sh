#!/usr/bin/bash

set -ux
mand=(25000 50000 100000)

for ((i=0;i<3;i++)) {
	luac -o "mand${mand[i]}" "benchmark/mand${mand[i]}.lua"
	for ((j=0;j<10;j++)) {
		time lua "mand${mand[i]}" > /dev/null
	}

	./optimizer "mand${mand[i]}" -o "optmand${mand[i]}"

	for ((j=0;j<10;j++)) {
		time lua "optmand${mand[i]}" > /dev/null
	}
}

