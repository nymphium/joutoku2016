import mkcfg, get_block from require'opeth.common.blockrealm'
import Reader, read from require'opeth.bytecode.reader'
import map, filter from require'opeth.common.utils'
print = (...) -> io.output!\write table.concat (map tostring, {...}), "\t"
inspect = require'inspect'

chk_cfg = (fnblock) ->
	cfg = mkcfg fnblock.instruction

	cfg_str = "\nassertion failed\n"..  inspect cfg

	for blk in *cfg
		if blk.start == 1
			assert (#blk.pred == 0), cfg_str .. "\nstart block must start at 1"
		else
			assert (#blk.pred > 0), cfg_str .. "\nblock(#{blk.start}, #{blk.end})'s `pred` is empty"

			for frm in *blk.pred
				map ((f) -> assert (#(filter ((succ) -> (tostring succ) == tostring frm), f) > 0), cfg_str .. "\nblock(#{frm.start}, #{frm.end})'s `succ` doesn't contain itself"), frm

		if blk.end == #fnblock.instruction
			assert (#blk.succ == 0), cfg_str .. "\nend block must end with 1"
		else
			assert (#blk.succ > 0), cfg_str .. "\nblock(#{blk.start}, #{blk.end})'s `succ` is empty"
			for succ in *blk.succ
				map ((t) -> assert (#(filter ((frm) -> (tostring frm) == tostring succ), t) > 0), cfg_str .. "\nblock(#{succ.start}, #{succ.end})'s `pred` doesn't contains itself"), succ

	cfg

prepNprint = (str) ->
	simplef = string.dump (assert loadstring str), true
	vmfmt = read Reader nil, simplef

	print inspect vmfmt.fnblock
	print "---------------\n------cfg------"

	with cfg = chk_cfg vmfmt.fnblock
		print inspect cfg

tblcmp = (t1, t2) -> (tostring t1) == (tostring t2)


describe "opeth.blockrealm, making control rlow graph", ->
	it "hoge test", ->
		cfg = prepNprint[[ ;
			local a = 1
			local cond  = false

			if cond then
				a = 2
			else
				a = 3
			end
			print(a)
		 ]]

	it "simple code test", ->
		cfg = prepNprint[[ ;
		local a = 1
		local b = 2
		print(a, b)
		]]

		with assert
			.is_equal 1, #cfg
			.is_equal 1, cfg[1].start
			.is_equal 0, #cfg[1].pred
			.is_equal 0, #cfg[1].succ
			.is_equal 7, cfg[1].end

	it "if then else test", ->
		cfg = prepNprint[[ ;
		local cond = true;
		if cond then
			print"OK"
		else
			print"NO"
		end
		]]
		
		with assert
			.is_equal 1, cfg[1].start
			.is_equal 10, cfg[4].end
			.is_equal 0, #cfg[1].pred

	it "for test", ->
		cfg = prepNprint[[ ;
		for i = 1, 10 do
			print(i)
		end
		]]

	it "while test", ->
		cfg = prepNprint[[ ;
		local i = 0
		while i < 10 do
			print(i)
			i = i + 1
		end
		]]

