describe "opeth.constant", ->
	import Reader, read from require'opeth.bytecode.reader'
	import rtype, rcst from require'opeth.opeth.common.constant'
	local src, fnblock

	setup ->
		src = assert io.open "spec/sample.out"
		-- _,  _, fnblock =  decode src
		{:fnblock} = read Reader src

	teardown -> src\close!

	it "research type test", ->
		with assert
			-- .is_equal "table", (rtype fnblock, {0, _, 10}, 4)
			-- .is_equal "function", (rtype fnblock, {0, _, 11}, 5)
			-- .is_equal "string", (rtype fnblock, {1, 3, 10}, 5)
			.is_equal "number", rtype fnblock, 1, 0

		src\seek "set"
	
	-- it "value fetch test", ->
		-- with assert
			-- -- TODO: too few to test
			-- .is_equal "^(", ({rcst fnblock, {1, 3, 10}, 5})[2]

		-- src\seek "set"

