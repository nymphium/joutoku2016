describe "bytecode read/write", ->
	import read, Reader from require'opeth.bytecode.reader'
	import write, Writer from require'opeth.bytecode.writer'
	local src

	setup ->
		src = assert io.open "spec/sample.out"

	teardown ->
		src\close!

	it "test", ->
		dst = io.tmpfile!

		wt = Writer dst
		write wt, read Reader src

		src\seek "set"

		assert.is_equal src\read"*a", wt\show!

