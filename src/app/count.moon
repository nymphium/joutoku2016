str = ""

while true
	if l = io.read!
		str ..= "#{l}\n"
	else
		break

op = {}

for l in str\gmatch"[^\n]+"
	if l\match "^[A-Z]+%s*%d+%.%d+$"
		o, time = l\match "([A-Z]+)%s*(.+)"

		unless op[o]
			op[o] = {
				time: tonumber time
				acc: 1
			}
		else
			with op[o]
				.time += tonumber time
				.acc += 1

for k, v in pairs op
	v.time *= 10000

	print "%-8s (%f / %8d): %08f"\format k, v.time, v.acc, v.time / v.acc

